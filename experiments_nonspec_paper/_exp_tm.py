#!/usr/bin/python3
from _basic_functions import *
from _plot_style import *
import copy

###########################
## HOST CONFIGURATION
###########################

host = shell_to_str('hostname').rstrip('\r\n ')

supports_htm = (host in ['jax', 'zyra', 'pyke'])
supports_papi = True

thread_counts = []
if      host == 'jax'   : thread_counts = [1, 6, 12, 18, 24, 36, 48, 60, 72, 96, 120, 144, 168, 190]
elif    host == 'zyra'  : thread_counts = [9, 18, 36, 72, 96, 140]
else                    : thread_counts = []

pinning_policy='-pin ' + shell_to_str('cd ../setbench/tools ; ./get_pinning_cluster.sh', exit_on_error=True)

#################################
## ALGORITHM/PLOT CONFIGURATION
#################################
# for good colors see:  https://pm1.narvii.com/7060/04cca64df6b6b0ccf24b22690e2fb796409abd79r1-1200-1800v2_uhq.jpg
# for line styles see:  https://matplotlib.org/_images/sphx_glr_line_styles_reference_001.png
# for markers see:      https://matplotlib.org/3.1.1/api/markers_api.html
# for hatches see:      https://kitchingroup.cheme.cmu.edu/media/2013-10-26-Hatched-symbols-in-matplotlib/hatched-symbols.png
#                       from https://kitchingroup.cheme.cmu.edu/blog/2013/10/26/Hatched-symbols-in-matplotlib
color_list = [
      '#fce205' # bumblebee
    , '#800000' # maroon
    , '#ff7aaa' ## light pink(?) # french rose
    , '#6f2da8' # grape
    , '#89cfef' # baby blue
    , '#4f7942' # fern
    , '#808080' # dark gray
    , '#202020' # dark dark gray
    , '#ffffff' # white
]

unfiltered_algs = dict({
          ''                                                        : dict(name=''                  , use=False     , hatch=''      , color=''              , line_style=''             , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_ext_bst_tm_auto.debra.norec'                       : dict(name='ext-norec'         , use=True      , hatch='/'     , color=color_list[0]   , line_style=(2,2)          , line_width='1'    , marker_style='.'      , marker_size=''        )
        , 'brown_ext_bst_tm_auto.debra.hybridnorec'                 : dict(name='ext-hynorec'       , use=True      , hatch='//'    , color=color_list[2]   , line_style=(2,2)          , line_width='1'    , marker_style='o'      , marker_size=''        )
        , 'brown_ext_bst_tm_auto.debra.rhnorec_post'                : dict(name='ext-rhnorec'       , use=False     , hatch='///'   , color=color_list[1]   , line_style=(2,2)          , line_width='1'    , marker_style='s'      , marker_size=''        )
        , 'brown_ext_bst_tm_auto.debra.rhnorec_post_pre'            : dict(name='ext-rh+'           , use=False     , hatch='////'  , color=color_list[1]   , line_style=(2,2)          , line_width='1'    , marker_style='P'      , marker_size=''        )
        , 'brown_ext_bst_tm_auto.debra.tl2'                         : dict(name='ext-tl2'           , use=True      , hatch='-'     , color=color_list[3]   , line_style=(4,4)          , line_width='1'    , marker_style='^'      , marker_size=''        )
        , 'brown_ext_bst_tm_auto.debra.hytm1'                       : dict(name='ext-tle'           , use=True      , hatch=' '     , color=color_list[7]   , line_style=(4,4)          , line_width='1'    , marker_style='*'      , marker_size=''        )
        , 'brown_ext_bst_tm_auto.debra.hytm2'                       : dict(name='ext-alg1'          , use=True      , hatch='*'     , color=color_list[8]   , line_style=(None,None)    , line_width='1'    , marker_style='d'      , marker_size=''        )
        , 'brown_ext_bst_tm_auto.debra.hytm2_3path'                 : dict(name='ext-alg1+'         , use=False     , hatch='**'    , color=color_list[8]   , line_style=(None,None)    , line_width='1'    , marker_style='P'      , marker_size=''        )
        , 'brown_ext_bst_tm_auto.debra.hytm3'                       : dict(name='ext-alg2'          , use=True      , hatch='o'     , color=color_list[5]   , line_style=(None,None)    , line_width='1'    , marker_style='X'      , marker_size=''        )
        , 'brown_int_bst_tm_auto.debra.norec'                       : dict(name='int-norec'         , use=False     , hatch='/'     , color=color_list[0]   , line_style=(2,2)          , line_width='1'    , marker_style='.'      , marker_size=''        )
        , 'brown_int_bst_tm_auto.debra.hybridnorec'                 : dict(name='int-hynorec'       , use=False     , hatch='//'    , color=color_list[2]   , line_style=(2,2)          , line_width='1'    , marker_style='o'      , marker_size=''        )
        , 'brown_int_bst_tm_auto.debra.rhnorec_post'                : dict(name='int-rhnorec'       , use=False     , hatch='///'   , color=color_list[1]   , line_style=(2,2)          , line_width='1'    , marker_style='s'      , marker_size=''        )
        , 'brown_int_bst_tm_auto.debra.rhnorec_post_pre'            : dict(name='int-rh+'           , use=False     , hatch='////'  , color=color_list[1]   , line_style=(2,2)          , line_width='1'    , marker_style='P'      , marker_size=''        )
        , 'brown_int_bst_tm_auto.debra.tl2'                         : dict(name='int-tl2'           , use=False     , hatch='-'     , color=color_list[3]   , line_style=(4,4)          , line_width='1'    , marker_style='^'      , marker_size=''        )
        , 'brown_int_bst_tm_auto.debra.hytm1'                       : dict(name='int-tle'           , use=False     , hatch=' '     , color=color_list[7]   , line_style=(4,4)          , line_width='1'    , marker_style='*'      , marker_size=''        )
        , 'brown_int_bst_tm_auto.debra.hytm2'                       : dict(name='int-alg1'          , use=False     , hatch='*'     , color=color_list[8]   , line_style=(None,None)    , line_width='1'    , marker_style='d'      , marker_size=''        )
        , 'brown_int_bst_tm_auto.debra.hytm2_3path'                 : dict(name='int-alg1+'         , use=False     , hatch='**'    , color=color_list[8]   , line_style=(None,None)    , line_width='1'    , marker_style='P'      , marker_size=''        )
        , 'brown_int_bst_tm_auto.debra.hytm3'                       : dict(name='int-alg2'          , use=False     , hatch='o'     , color=color_list[5]   , line_style=(None,None)    , line_width='1'    , marker_style='X'      , marker_size=''        )
        , 'brown_sigouin_int_avl_tm_auto.debra.norec'               : dict(name='avl-norec'         , use=False     , hatch='/'     , color=color_list[0]   , line_style=(2,2)          , line_width='1'    , marker_style='.'      , marker_size=''        )
        , 'brown_sigouin_int_avl_tm_auto.debra.hybridnorec'         : dict(name='avl-hynorec'       , use=False     , hatch='//'    , color=color_list[2]   , line_style=(2,2)          , line_width='1'    , marker_style='o'      , marker_size=''        )
        , 'brown_sigouin_int_avl_tm_auto.debra.rhnorec_post'        : dict(name='avl-rhnorec'       , use=False     , hatch='///'   , color=color_list[1]   , line_style=(2,2)          , line_width='1'    , marker_style='s'      , marker_size=''        )
        , 'brown_sigouin_int_avl_tm_auto.debra.rhnorec_post_pre'    : dict(name='avl-rh+'           , use=False     , hatch='////'  , color=color_list[1]   , line_style=(2,2)          , line_width='1'    , marker_style='P'      , marker_size=''        )
        , 'brown_sigouin_int_avl_tm_auto.debra.tl2'                 : dict(name='avl-tl2'           , use=False     , hatch='-'     , color=color_list[3]   , line_style=(4,4)          , line_width='1'    , marker_style='^'      , marker_size=''        )
        , 'brown_sigouin_int_avl_tm_auto.debra.hytm1'               : dict(name='avl-tle'           , use=False     , hatch=' '     , color=color_list[7]   , line_style=(4,4)          , line_width='1'    , marker_style='*'      , marker_size=''        )
        , 'brown_sigouin_int_avl_tm_auto.debra.hytm2'               : dict(name='avl-alg1'          , use=False     , hatch='*'     , color=color_list[8]   , line_style=(None,None)    , line_width='1'    , marker_style='d'      , marker_size=''        )
        , 'brown_sigouin_int_avl_tm_auto.debra.hytm2_3path'         : dict(name='avl-alg1+'         , use=False     , hatch='**'    , color=color_list[8]   , line_style=(None,None)    , line_width='1'    , marker_style='P'      , marker_size=''        )
        , 'brown_sigouin_int_avl_tm_auto.debra.hytm3'               : dict(name='avl-alg2'          , use=False     , hatch='o'     , color=color_list[5]   , line_style=(None,None)    , line_width='1'    , marker_style='X'      , marker_size=''        )
        , 'brown_abtree_tm_auto.debra.norec'                        : dict(name='abtree-norec'      , use=False     , hatch='/'     , color=color_list[0]   , line_style=(2,2)          , line_width='1'    , marker_style='.'      , marker_size=''        )
        , 'brown_abtree_tm_auto.debra.hybridnorec'                  : dict(name='abtree-hynorec'    , use=False     , hatch='//'    , color=color_list[2]   , line_style=(2,2)          , line_width='1'    , marker_style='o'      , marker_size=''        )
        , 'brown_abtree_tm_auto.debra.rhnorec_post'                 : dict(name='abtree-rhnorec'    , use=False     , hatch='///'   , color=color_list[1]   , line_style=(2,2)          , line_width='1'    , marker_style='s'      , marker_size=''        )
        , 'brown_abtree_tm_auto.debra.rhnorec_post_pre'             : dict(name='abtree-rh+'        , use=False     , hatch='////'  , color=color_list[1]   , line_style=(2,2)          , line_width='1'    , marker_style='P'      , marker_size=''        )
        , 'brown_abtree_tm_auto.debra.tl2'                          : dict(name='abtree-tl2'        , use=False     , hatch='-'     , color=color_list[3]   , line_style=(4,4)          , line_width='1'    , marker_style='^'      , marker_size=''        )
        , 'brown_abtree_tm_auto.debra.hytm1'                        : dict(name='abtree-tle'        , use=False     , hatch=' '     , color=color_list[7]   , line_style=(4,4)          , line_width='1'    , marker_style='*'      , marker_size=''        )
        , 'brown_abtree_tm_auto.debra.hytm2'                        : dict(name='abtree-alg1'       , use=False     , hatch='*'     , color=color_list[8]   , line_style=(None,None)    , line_width='1'    , marker_style='d'      , marker_size=''        )
        , 'brown_abtree_tm_auto.debra.hytm2_3path'                  : dict(name='abtree-alg1+'      , use=False     , hatch='**'    , color=color_list[8]   , line_style=(None,None)    , line_width='1'    , marker_style='P'      , marker_size=''        )
        , 'brown_abtree_tm_auto.debra.hytm3'                        : dict(name='abtree-alg2'       , use=False     , hatch='o'     , color=color_list[5]   , line_style=(None,None)    , line_width='1'    , marker_style='X'      , marker_size=''        )
})

#################################
## HELPER FUNCTIONS
#################################

short_to_long = dict()
for k,v in unfiltered_algs.items():
    if k and v and v['name']:
        short_to_long[v['name']] = k

## reindex styles by short alg and style property name
short_to_style_props = dict()
for prop in list(list(unfiltered_algs.values())[0].keys()):
    # print('prop={}'.format(prop))
    short_to_style_props[prop] = dict({short_name: unfiltered_algs[short_to_long[short_name]][prop] for short_name in short_to_long.keys()})

def uses_hardware_tm(long_name):
    return 'hytm' in long_name or 'rhnorec' in long_name or 'hybrid' in long_name or 'htm' in long_name

filtered_algs = dict()
for k,v in unfiltered_algs.items():
    if k and v and v['name'] and v['use'] and (supports_htm or not uses_hardware_tm(k)):
        filtered_algs[k] = v['name']

def extract_filtered_algs(exp_dict, file_name, field_name):
    result = grep_line(exp_dict, file_name, 'algorithm')
    if result in filtered_algs.keys():
        return filtered_algs[result]
    return ''

def extract_elapsed_millis(exp_dict, file_name, field_name):
    return get_best_typecast(shell_to_str('cat {} | grep "elapsed milliseconds" | cut -d":" -f2 | tr -d " "'.format(file_name)))

## beware: should copy.deepcopy() this to use it
base_config = dict(
      dummy = None
    , dark_mode = False
    , legend_columns = 3
    , height_inches = 4
    , hatch_map = short_to_style_props['hatch']
    , color_map = short_to_style_props['color']
    , marker_style_map = short_to_style_props['marker_style']
    , line_style_map = short_to_style_props['line_style']
    , legend_markerscale = 3
    , font_size = 24
    , xtitle = None
)

def extract_global_lock_sec(exp_dict, file_name, field_name):
    return get_best_typecast(shell_to_str("cat "+file_name+" | grep -E 'seconds global lock is held[ ]+:' | cut -d':' -f2 | awk '{sum+=$1}END{print sum}'"))

def extract_total_commits(exp_dict, file_name, field_name):
    return get_best_typecast(shell_to_str("cat "+file_name+" | grep -E 'total.*commit[ ]+:' | cut -d':' -f2 | awk '{sum+=$1}END{print sum}'"))

def extract_total_aborts(exp_dict, file_name, field_name):
    return get_best_typecast(shell_to_str("cat "+file_name+" | grep -E 'total.*abort[ ]+:' | cut -d':' -f2 | awk '{sum+=$1}END{print sum}'"))

def extract_abort_rate(exp_dict, file_name, field_name):
    commits = extract_total_commits(exp_dict, file_name, field_name)
    aborts = extract_total_aborts(exp_dict, file_name, field_name)
    if aborts and commits and isinstance(aborts, int) and isinstance(commits, int):
        return 100 * (aborts / (commits+aborts))
    return 0

#################################
## DEFINING THE EXPERIMENT
#################################

def define_experiment(exp_dict, args):
    set_dir_compile  (exp_dict, os.getcwd() + '/../')
    set_dir_tools    (exp_dict, os.getcwd() + '/../setbench/tools')
    set_dir_run      (exp_dict, os.getcwd() + '/bin')
    set_dir_data     (exp_dict, os.getcwd() + '/data_tm')

    cmd_compile = 'make bin_dir={__dir_run} -j'
    if not supports_papi: cmd_compile += ' has_libpapi=0'

    alg_groups = dict()
    # alg_groups['abtree_tm']     = list(filter(lambda name: ('abtree' in name or 'bwtree' in name)   , filtered_algs.values()))
    alg_groups['ext_tm']        = list(filter(lambda name: ('ext' in name)                          , filtered_algs.values()))
    # alg_groups['int_tm']        = list(filter(lambda name: ('int' in name)                          , filtered_algs.values()))
    # alg_groups['avl_tm']        = list(filter(lambda name: ('avl' in name)                          , filtered_algs.values()))
    algorithms_to_run           = list(set([short_to_long[short_name] for group in alg_groups.values() for short_name in group]))

    add_run_param    (exp_dict, 'INS_DEL_FRAC'      , ['0.0 0.0', '5.0 5.0', '20.0 20.0'])
    add_run_param    (exp_dict, 'MAXKEY'            , [100000])
    add_run_param    (exp_dict, 'algorithm'         , algorithms_to_run)
    add_run_param    (exp_dict, 'thread_pinning'    , [pinning_policy])
    add_run_param    (exp_dict, 'millis'            , [10000])
    add_run_param    (exp_dict, 'rqsize'            , [256])
    add_run_param    (exp_dict, 'nrq'               , [0, 1])
    add_run_param    (exp_dict, 'TOTAL_THREADS'     , thread_counts)
    add_run_param    (exp_dict, '__trials'          , [1, 2, 3, 4, 5, 6])

    cmd_run = 'LD_PRELOAD=../../setbench/lib/libjemalloc.so timeout 60 numactl -i all {__time_cmd} ./{algorithm} -nrq {nrq} -nwork $(expr {TOTAL_THREADS} - {nrq}) -nprefill {TOTAL_THREADS} -prefill-hybrid -prefill-hybrid-min-ms 1000 -prefill-hybrid-max-ms 5000 -insdel {INS_DEL_FRAC} -rqsize {rqsize} -rq 0 -k {MAXKEY} -t {millis} {thread_pinning}'
    if args.testing:
        cmd_compile += ' use_asserts=1'
        add_run_param(exp_dict, '__trials'          , [1])
        add_run_param(exp_dict, 'TOTAL_THREADS'     , [thread_counts[0], thread_counts[-1]])
        add_run_param(exp_dict, 'millis'            , [200])
        cmd_run = cmd_run.replace('-nprefill {TOTAL_THREADS}', '-nprefill 0')
        # add_run_param(exp_dict, 'INS_DEL_FRAC'      , ['20.0 20.0'])

    set_cmd_compile  (exp_dict, cmd_compile)
    set_cmd_run      (exp_dict, cmd_run) ## also adds data_fields to capture the outputs of the `time` command

    add_data_field   (exp_dict, 'alg'               , coltype='TEXT', extractor=extract_filtered_algs)
    add_data_field   (exp_dict, 'validate_result'   , coltype='TEXT', validator=is_equal('success'))
    add_data_field   (exp_dict, 'total_throughput'  , coltype='INTEGER', validator=is_positive)
    add_data_field   (exp_dict, 'PAPI_L3_TCM'       , coltype='REAL')
    add_data_field   (exp_dict, 'PAPI_L2_TCM'       , coltype='REAL')
    add_data_field   (exp_dict, 'PAPI_TOT_CYC'      , coltype='REAL')
    add_data_field   (exp_dict, 'PAPI_TOT_INS'      , coltype='REAL')
    add_data_field   (exp_dict, 'elapsed_millis'    , extractor=extract_elapsed_millis, validator=is_positive)
    add_data_field   (exp_dict, 'MILLIS_TO_RUN'     , coltype='TEXT', validator=is_positive)
    add_data_field   (exp_dict, 'RECLAIM'           , coltype='TEXT')
    add_data_field   (exp_dict, 'tree_stats_height' , coltype='INTEGER', validator=is_optional)
    add_data_field   (exp_dict, 'tree_stats_avgKeyDepth' , coltype='REAL', validator=is_optional)
    add_data_field   (exp_dict, 'total_commits'     , coltype='INTEGER', extractor=extract_total_commits, validator=is_optional)
    add_data_field   (exp_dict, 'total_aborts'      , coltype='INTEGER', extractor=extract_total_aborts, validator=is_optional)
    add_data_field   (exp_dict, 'abort_rate'        , coltype='REAL', extractor=extract_abort_rate, validator=is_optional)
    add_data_field   (exp_dict, 'global_lock_sec'   , coltype='REAL', extractor=extract_global_lock_sec, validator=is_optional)

    ## create pages of plots (one page per combination of algorithm-group and value-field)
    for group_name in alg_groups.keys():
        group = alg_groups[group_name]

        config_for_group = copy.deepcopy(base_config)
        config_for_group['series_order'] = group

        filter_sql = 'alg in ({})'.format(','.join(["'"+x+"'" for x in group]))

        ## create legend to put below each table of plots
        # add_plot_set(exp_dict, name=group_name+'-legend.png', series='alg', x_axis='TOTAL_THREADS', y_axis='total_throughput', filter=filter_sql, plot_type=plot_bars_legend, plot_cmd_args=config_for_group)
        add_plot_set(exp_dict
            , name=group_name+'-legend.png'
            , series='alg'
            , x_axis='TOTAL_THREADS'
            , y_axis='total_throughput'
            , filter=filter_sql
            # , plot_type=plot_bars_legend
            , plot_type=plot_line_regions_legend
            , plot_cmd_args=config_for_group
        )

        value_fields = ['total_throughput', 'abort_rate', 'global_lock_sec']

        ## add papi outputs where they are not supported
        papi_fields = []
        if supports_papi:
            papi_fields = ['PAPI_L2_TCM', 'PAPI_L3_TCM', 'PAPI_TOT_CYC', 'PAPI_TOT_INS']
            value_fields += papi_fields

        value_fields += ['tree_stats_avgKeyDepth']

        ## add pages for fields extracted from the time command
        value_fields += get_time_fields(exp_dict)
        if 'faults_major' in value_fields: value_fields.remove('faults_major')

        for field in value_fields:
            add_plot_set(exp_dict
                , name=group_name+'_'+field+'-k{MAXKEY}-u{INS_DEL_FRAC}-nrq{nrq}.png'
                , title=field
                , varying_cols_list=['MAXKEY', 'INS_DEL_FRAC', 'nrq']
                , filter=filter_sql
                , series='alg'
                , x_axis='TOTAL_THREADS'
                , y_axis=field
                # , plot_type=plot_bars
                , plot_type=plot_line_regions
                , plot_cmd_args=config_for_group
            )

        ## for each group do factor-analysis for: number of range updaters vs update rate
        # print('calling add_page_set here')
        add_page_set(
              exp_dict
            , image_files=group_name+'_{row_field}-k{MAXKEY}-u{INS_DEL_FRAC}-nrq{nrq}.png'
            , name=group_name+'_factors'
            , column_field='nrq'
            , row_field=value_fields
            , page_field_list=['MAXKEY', 'INS_DEL_FRAC']
            , legend_file=group_name+'-legend.png'
        )
