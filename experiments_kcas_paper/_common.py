from _basic_functions import *
import copy

###########################
## HOST CONFIGURATION
###########################

#######################################################################
# INCLUDING SOME HACKS TO DEAL WITH INDIRECT CONNECTION TO NASUS !!!! #
#######################################################################

host = shell_to_str('hostname').rstrip('\r\n ')
if host == 'rift': host = 'nasus' #####################################

supports_htm = (host in ['jax', 'zyra', 'pyke'])
supports_papi = (host != 'nasus')

thread_counts = []
if host == 'nasus': thread_counts = [64, 128, 192, 256]
elif host == 'jax': thread_counts = [48, 96, 144, 190]
else:               thread_counts = []

pinning_policy='-pin ' + shell_to_str('cd ../setbench/tools ; ./get_pinning_cluster.sh', exit_on_error=True)
if host == 'nasus': ####################################################
    pinning_policy='-pin 0-63,128-191,64-127,192-255'

#################################
## ALGORITHM/PLOT CONFIGURATION
#################################

## matplotlib hatching types (can use multiple)
#
#     /   - diagonal hatching
#     \   - back diagonal
#     |   - vertical
#     -   - horizontal
#     +   - crossed
#     x   - crossed diagonal
#     o   - small circle
#     O   - large circle
#     .   - dots
#     *   - stars
#
# colors: https://pm1.narvii.com/7060/04cca64df6b6b0ccf24b22690e2fb796409abd79r1-1200-1800v2_uhq.jpg

color_list = [
      '#fce205' # bumblebee
    , '#800000' # maroon
    , '#f64a8a' # french rose
    , '#6f2da8' # grape
    , '#89cfef' # baby blue
    , '#4f7942' # fern
]

unfiltered_algs = dict({
          ''                                                        : dict(name=''                  , use=False     , hatch=''      , color=''              , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_int_bst_tm_auto.debra.norec'                       : dict(name='int-bst-norec'     , use=True      , hatch='/'     , color=color_list[0]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_int_bst_tm_auto.debra.hybridnorec'                 : dict(name='int-bst-hynorec'   , use=True      , hatch='//'    , color=color_list[1]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_int_bst_tm_auto.debra.rhnorec_post'                : dict(name='int-bst-rhnorec'   , use=True      , hatch='///'   , color=color_list[2]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_int_bst_tm_auto.debra.rhnorec_post_pre'            : dict(name='int-bst-rh+'       , use=False     , hatch='////'  , color=color_list[2]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_int_bst_tm_auto.debra.tl2'                         : dict(name='int-bst-tl2'       , use=True      , hatch='-'     , color=color_list[3]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_int_bst_tm_auto.debra.hytm1'                       : dict(name='int-bst-tle'       , use=True      , hatch=' '     , color=color_list[4]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_int_bst_tm_auto.debra.hytm2'                       : dict(name='int-bst-br1'       , use=False     , hatch='*'     , color=color_list[5]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_sigouin_int_avl_tm_auto.debra.norec'               : dict(name='int-avl-norec'     , use=True      , hatch='/'     , color=color_list[0]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_sigouin_int_avl_tm_auto.debra.hybridnorec'         : dict(name='int-avl-hynorec'   , use=True      , hatch='//'    , color=color_list[1]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_sigouin_int_avl_tm_auto.debra.rhnorec_post'        : dict(name='int-avl-rhnorec'   , use=True      , hatch='///'   , color=color_list[2]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_sigouin_int_avl_tm_auto.debra.rhnorec_post_pre'    : dict(name='int-avl-rh+'       , use=False     , hatch='////'  , color=color_list[2]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_sigouin_int_avl_tm_auto.debra.tl2'                 : dict(name='int-avl-tl2'       , use=True      , hatch='-'     , color=color_list[3]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_sigouin_int_avl_tm_auto.debra.hytm1'               : dict(name='int-avl-tle'       , use=True      , hatch=' '     , color=color_list[4]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_sigouin_int_avl_tm_auto.debra.hytm2'               : dict(name='int-avl-br1'       , use=False     , hatch='*'     , color=color_list[5]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_abtree_tm_auto.debra.norec'                        : dict(name='abtree-norec'      , use=True      , hatch='/'     , color=color_list[0]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_abtree_tm_auto.debra.hybridnorec'                  : dict(name='abtree-hynorec'    , use=True      , hatch='//'    , color=color_list[1]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_abtree_tm_auto.debra.rhnorec_post'                 : dict(name='abtree-rhnorec'    , use=True      , hatch='///'   , color=color_list[2]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_abtree_tm_auto.debra.rhnorec_post_pre'             : dict(name='abtree-rh+'        , use=False     , hatch='////'  , color=color_list[2]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_abtree_tm_auto.debra.tl2'                          : dict(name='abtree-tl2'        , use=True      , hatch='-'     , color=color_list[3]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_abtree_tm_auto.debra.hytm1'                        : dict(name='abtree-tle'        , use=True      , hatch=' '     , color=color_list[4]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_abtree_tm_auto.debra.hytm2'                        : dict(name='abtree-br1'        , use=False     , hatch='*'     , color=color_list[5]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'wang_openbwtree'                                         : dict(name='open-bwtree'       , use=True      , hatch='xx'    , color=color_list[0]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_ext_abtree_lf.debra'                               : dict(name='abtree-lf'         , use=True      , hatch='*'     , color=color_list[1]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_sigouin_abtree_kcas.debra'                         : dict(name='abtree-kcas'       , use=False     , hatch='o'     , color=color_list[5]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_sigouin_abtree_kcas_htm.debra'                     : dict(name='abtree-kcas+'      , use=False     , hatch='O'     , color=color_list[5]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_sigouin_abtree_kcas_validate.debra'                : dict(name='abtree-pathcas'    , use=True      , hatch='o'     , color=color_list[5]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_sigouin_abtree_kcas_validate_htm.debra'            : dict(name='abtree-pathcas+'   , use=True      , hatch='O'     , color=color_list[5]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'ellen_ext_bst_lf.debra'                                  : dict(name='ext-bst-lf'        , use=True      , hatch='.'     , color=color_list[0]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'guerraoui_ext_bst_ticket.debra'                          : dict(name='ext-bst-locks'     , use=True      , hatch='..'    , color=color_list[1]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'natarajan_ext_bst_lf.debra'                              : dict(name='ext-bst-lf2'       , use=True      , hatch='...'   , color=color_list[2]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'drachsler_pext_bst_lock.debra'                           : dict(name='pext-bst-locks'    , use=True      , hatch='....'  , color=color_list[3]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'howley_int_bst_lf.debra'                                 : dict(name='int-bst-lf'        , use=False     , hatch='|'     , color=color_list[4]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'ramachandran_int_bst_lf.debra'                           : dict(name='int-bst-lf2'       , use=False     , hatch='||'    , color=color_list[5]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'sigouin_int_bst_kcas.debra'                              : dict(name='int-bst-kcas'      , use=False     , hatch='o'     , color=color_list[5]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'sigouin_int_bst_kcas_htm.debra'                          : dict(name='int-bst-kcas+'     , use=False     , hatch='O'     , color=color_list[5]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'sigouin_int_bst_kcas_validate.debra'                     : dict(name='int-bst-pathcas'   , use=True      , hatch='o'     , color=color_list[5]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'sigouin_int_bst_kcas_validate_htm.debra'                 : dict(name='int-bst-pathcas+'  , use=True      , hatch='O'     , color=color_list[5]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'brown_ext_chromatic_lf.debra'                            : dict(name='ext-chromatic-lf'  , use=True      , hatch='.'     , color=color_list[0]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'bronson_pext_bst_occ.debra'                              : dict(name='pext-avl-occ'      , use=True      , hatch='..'    , color=color_list[1]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'sigouin_int_avl_kcas.debra'                              : dict(name='int-avl-kcas'      , use=False     , hatch='o'     , color=color_list[5]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'sigouin_int_avl_kcas_htm.debra'                          : dict(name='int-avl-kcas+'     , use=False     , hatch='O'     , color=color_list[5]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'sigouin_int_avl_kcas_validate.debra'                     : dict(name='int-avl-pathcas'   , use=True      , hatch='o'     , color=color_list[5]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
        , 'sigouin_int_avl_kcas_validate_htm.debra'                 : dict(name='int-avl-pathcas+'  , use=True      , hatch='O'     , color=color_list[5]   , line_style=''     , line_width=''     , marker_style=''       , marker_size=''        )
})

short_to_long = dict()
for k,v in unfiltered_algs.items():
    if k and v and v['name']:
        short_to_long[v['name']] = k

## reindex styles by short alg and style property name
short_to_style_props = dict()
for prop in list(list(unfiltered_algs.values())[0].keys()):
    # print('prop={}'.format(prop))
    short_to_style_props[prop] = dict({short_name: unfiltered_algs[short_to_long[short_name]][prop] for short_name in short_to_long.keys()})
# print('short_to_style_props={}'.format(short_to_style_props))

def uses_hardware_tm(long_name):
    return 'hytm' in long_name or 'rhnorec' in long_name or 'hybrid' in long_name or 'htm' in long_name

def is_tm_alg_long(long_name):
    return 'hytm' in long_name or 'norec' in long_name or 'tl2' in long_name or 'htm' in long_name

def is_tm_alg_short(short_name):
    return '-tle' in short_name or '-tl2' in short_name or '-br1' in short_name or 'norec' in short_name or '+' in short_name

def is_our_alg_short(short_name):
    return '-kcas' in short_name or '-pathcas' in short_name

filtered_algs = dict()
for k,v in unfiltered_algs.items():
    if k and v and v['name'] and v['use'] and (supports_htm or not uses_hardware_tm(k)):
        filtered_algs[k] = v['name']
# print(filtered_algs)

def extract_filtered_algs(exp_dict, file_name, field_name):
    result = grep_line(exp_dict, file_name, 'algorithm')
    if result in filtered_algs.keys():
        return filtered_algs[result]
    return ''

def extract_elapsed_millis(exp_dict, file_name, field_name):
    return get_best_typecast(shell_to_str('cat {} | grep "elapsed milliseconds" | cut -d":" -f2 | tr -d " "'.format(file_name)))

## beware: should copy.deepcopy() this to use it
base_config = dict(
      dummy = None
    , dark_mode = False
    , legend_columns = 3
    , height_inches = 4
    , hatch_map = short_to_style_props['hatch']
    , color_map = short_to_style_props['color']
    , font_size = 24
    , xtitle = None
)
