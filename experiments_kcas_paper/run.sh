#!/bin/bash

if [ "$#" -ne "1" ] ; then
    mode=""
else
    mode="--testing"
fi

../setbench/tools/data_framework/run_experiment.py _exp_non_tm.py -crdpw $mode
mv output_log.txt output_log_non_tm.txt
../setbench/tools/data_framework/run_experiment.py _exp_tm.py -crdpw $mode
mv output_log.txt output_log_tm.txt
