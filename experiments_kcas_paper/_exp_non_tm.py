#!/usr/bin/python3
from _basic_functions import *
from _plot_style import *
from _common import *
import copy

def define_experiment(exp_dict, args):
    set_dir_compile  (exp_dict, os.getcwd() + '/../')
    set_dir_tools    (exp_dict, os.getcwd() + '/../setbench/tools')
    set_dir_run      (exp_dict, os.getcwd() + '/bin')
    set_dir_data     (exp_dict, os.getcwd() + '/data_non_tm')

    cmd_compile = 'make bin_dir={__dir_run} -j'
    if not supports_papi: cmd_compile += ' has_libpapi=0'

    alg_groups = dict()
    alg_groups['abtree_hc']     = list(filter(lambda name: ('abtree' in name or 'bwtree' in name) and not is_tm_alg_short(name) , filtered_algs.values()))
    alg_groups['bst_hc']        = list(filter(lambda name: ('bst' in name) and not is_tm_alg_short(name)                        , filtered_algs.values()))
    alg_groups['avl_hc']        = list(filter(lambda name: ('avl' in name or 'chromatic' in name) and not is_tm_alg_short(name) , filtered_algs.values()))
    algorithms_to_run           = list(set([short_to_long[short_name] for group in alg_groups.values() for short_name in group]))

    add_run_param    (exp_dict, 'INS_DEL_FRAC'      , ['0.5 0.5', '5.0 5.0', '50.0 50.0'])
    add_run_param    (exp_dict, 'MAXKEY'            , [20000000, 2000000, 200000])
    add_run_param    (exp_dict, 'algorithm'         , algorithms_to_run)
    add_run_param    (exp_dict, 'thread_pinning'    , [pinning_policy])
    add_run_param    (exp_dict, 'millis'            , [10000])
    add_run_param    (exp_dict, 'TOTAL_THREADS'     , thread_counts)
    add_run_param    (exp_dict, '__trials'          , [1, 2, 3, 4, 5, 6])

    cmd_run = 'LD_PRELOAD=../../setbench/lib/libjemalloc.so timeout 60 numactl -i all {__time_cmd} ./{algorithm} -nwork {TOTAL_THREADS} -nprefill {TOTAL_THREADS} -prefill-hybrid -prefill-hybrid-min-ms 1000 -prefill-hybrid-max-ms 5000 -insdel {INS_DEL_FRAC} -k {MAXKEY} -t {millis} {thread_pinning}'
    if args.testing:
        add_run_param(exp_dict, '__trials'          , [1])
        add_run_param(exp_dict, 'TOTAL_THREADS'     , shell_to_listi('cd ' + get_dir_tools(exp_dict) + ' ; ./get_thread_counts_max.sh', exit_on_error=True))
        add_run_param(exp_dict, 'millis'            , [100])
        cmd_compile += ' use_asserts=1'
        cmd_run = cmd_run.replace('-nprefill {TOTAL_THREADS}', '-nprefill 0')

    set_cmd_compile  (exp_dict, cmd_compile)
    set_cmd_run      (exp_dict, cmd_run) ## also adds data_fields to capture the outputs of the `time` command

    add_data_field   (exp_dict, 'alg'               , coltype='TEXT', extractor=extract_filtered_algs)
    add_data_field   (exp_dict, 'validate_result'   , coltype='TEXT', validator=is_equal('success'))
    add_data_field   (exp_dict, 'total_throughput'  , coltype='INTEGER', validator=is_positive)
    add_data_field   (exp_dict, 'PAPI_L3_TCM'       , coltype='REAL')
    add_data_field   (exp_dict, 'PAPI_L2_TCM'       , coltype='REAL')
    add_data_field   (exp_dict, 'PAPI_TOT_CYC'      , coltype='REAL')
    add_data_field   (exp_dict, 'PAPI_TOT_INS'      , coltype='REAL')
    add_data_field   (exp_dict, 'elapsed_millis'    , extractor=extract_elapsed_millis, validator=is_positive)
    add_data_field   (exp_dict, 'MILLIS_TO_RUN'     , coltype='TEXT', validator=is_positive)
    add_data_field   (exp_dict, 'RECLAIM'           , coltype='TEXT')
    add_data_field   (exp_dict, 'tree_stats_height' , coltype='REAL', validator=is_optional)
    add_data_field   (exp_dict, 'tree_stats_avgKeyDepth' , coltype='REAL', validator=is_optional)

    ## create pages of plots (one page per combination of algorithm-group and value-field)
    for group_name in alg_groups.keys():
        group = alg_groups[group_name]

        config_for_group = copy.deepcopy(base_config)
        config_for_group['series_order'] = group

        config_debug_legend = copy.deepcopy(base_config)
        config_debug_legend['series_order'] = group
        config_debug_legend['legend_columns'] = 1

        filter_sql = 'alg in ({})'.format(','.join(["'"+x+"'" for x in group]))

        ## create legend to put below each table of plots
        # add_plot_set(exp_dict, name=group_name+'-legend.png', series='alg', x_axis='TOTAL_THREADS', y_axis='total_throughput', filter=filter_sql, plot_type=plot_line_regions_legend)
        # add_plot_set(exp_dict, name=group_name+'-legend.png', series='alg', x_axis='TOTAL_THREADS', y_axis='total_throughput', filter=filter_sql, plot_type='bars', plot_cmd_args='--legend-only --legend-col 3')
        add_plot_set(exp_dict, name=group_name+'-legend.png', series='alg', x_axis='TOTAL_THREADS', y_axis='total_throughput', filter=filter_sql, plot_type=plot_bars_legend, plot_cmd_args=config_for_group)
        add_plot_set(exp_dict, name=group_name+'-debug-legend.png', series='alg', x_axis='TOTAL_THREADS', y_axis='total_throughput', filter=filter_sql, plot_type=plot_bars_legend, plot_cmd_args=config_debug_legend)

        value_fields = ['total_throughput']

        ## add papi outputs where they are not supported
        papi_fields = []
        if supports_papi:
            papi_fields = ['PAPI_L2_TCM', 'PAPI_L3_TCM', 'PAPI_TOT_CYC', 'PAPI_TOT_INS']
            value_fields += papi_fields

        value_fields += ['tree_stats_avgKeyDepth']

        ## add pages for fields extracted from the time command
        value_fields += get_time_fields(exp_dict)
        if 'faults_major' in value_fields: value_fields.remove('faults_major')

        for field in value_fields:
            add_plot_set(exp_dict
                , name=group_name+'_'+field+'-u{INS_DEL_FRAC}-k{MAXKEY}.png'
                , title=field
                , varying_cols_list=['INS_DEL_FRAC', 'MAXKEY']
                , filter=filter_sql
                , series='alg'
                , x_axis='TOTAL_THREADS'
                , y_axis=field
                # , plot_type='bars'
                # , plot_type=plot_line_regions
                , plot_type=plot_bars
                , plot_cmd_args=config_for_group
            )

        ## for each group cf. workloads (with value fields as tables)
        add_page_set(
              exp_dict
            , image_files=group_name+'_{table_field}-u{INS_DEL_FRAC}-k{MAXKEY}.png'
            , name=group_name+'_workloads'
            , column_field='INS_DEL_FRAC'
            , row_field='MAXKEY'
            , table_field=value_fields
            , legend_file=group_name+'-legend.png'
        )

        ## for each group cf. maxkey and value fields (with MAXKEY as tables)
        add_page_set(
              exp_dict
            , image_files=group_name+'_{row_field}-u{INS_DEL_FRAC}-k{MAXKEY}.png'
            , name=group_name+'_factors'
            , column_field='INS_DEL_FRAC'
            , row_field=value_fields
            , table_field='MAXKEY'
            , legend_file=group_name+'-legend.png'
        )
