#!/bin/bash

mkdir output_txt 2>/dev/null
for x in $(./_find_html_files.sh) ; do
	f=$x
	f=${f//\.\//}
	f=${f//\//_}
	f=${f%.*}
	./_extract_ordered_pngnames.sh $x > output_txt/${f}.txt
done
#echo "done: see output_txt/"
