#!/bin/bash

if [ "$#" -eq "0" ]; then
    echo "USAGE: $(basename $0) TESTING_1_OR_0 REMOTE_HOST_NAME [REMOTE_HOST_NAME [...]]"
    exit 1
fi

testing=$1
shift
if [ "$testing" != "1" ] && [ "$testing" != "0" ]; then
    echo "must specify first argument TESTING_0_OR_1 properly"
    exit 1
fi

date
dir=$(pwd)
for host in $@ ; do
    if [ "$testing" == "1" ] ; then
        testing_str='testing'
    else
        testing_str=''
    fi
    echo "## cmd to run on ${host}: ssh -tt $host \"bash -c 'cd ${dir}/.. ; ./run.sh $testing_str'\" &"
    ssh -tt $host "bash -c 'cd ${dir}/.. ; ./run.sh $testing_str'" &
done

date
echo "## waiting for remote jobs to end"
wait
date

./process_remotes.sh $testing $@
