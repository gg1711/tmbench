#!/usr/bin/python3
from _basic_functions import *

unfiltered_algs = dict({
              '' : ''
            # , 'brown_int_bst_tm_auto.debra.norec'                       : 'int-bst-norec'
            # , 'brown_int_bst_tm_auto.debra.hybridnorec'                 : 'int-bst-hynorec'
            # , 'brown_int_bst_tm_auto.debra.rhnorec_post'                : 'int-bst-rh'
            # , 'brown_int_bst_tm_auto.debra.rhnorec_post_pre'            : 'int-bst-rh+'
            # , 'brown_int_bst_tm_auto.debra.tl2'                         : 'int-bst-tl2'
            # , 'brown_int_bst_tm_auto.debra.hytm1'                       : 'int-bst-tle'
            # , 'brown_int_bst_tm_auto.debra.hytm2'                       : 'int-bst-br1'
            # , 'brown_sigouin_int_avl_tm_auto.debra.norec'               : 'int-avl-norec'
            # , 'brown_sigouin_int_avl_tm_auto.debra.hybridnorec'         : 'int-avl-hynorec'
            # , 'brown_sigouin_int_avl_tm_auto.debra.rhnorec_post'        : 'int-avl-rh'
            # , 'brown_sigouin_int_avl_tm_auto.debra.rhnorec_post_pre'    : 'int-avl-rh+'
            # , 'brown_sigouin_int_avl_tm_auto.debra.tl2'                 : 'int-avl-tl2'
            # , 'brown_sigouin_int_avl_tm_auto.debra.hytm1'               : 'int-avl-tle'
            # , 'brown_sigouin_int_avl_tm_auto.debra.hytm2'               : 'int-avl-br1'
            # , 'brown_abtree_tm_auto.debra.norec'                        : 'abtree-norec'
            # , 'brown_abtree_tm_auto.debra.hybridnorec'                  : 'abtree-hynorec'
            # , 'brown_abtree_tm_auto.debra.rhnorec_post'                 : 'abtree-rh'
            # , 'brown_abtree_tm_auto.debra.rhnorec_post_pre'             : 'abtree-rh+'
            # , 'brown_abtree_tm_auto.debra.tl2'                          : 'abtree-tl2'
            # , 'brown_abtree_tm_auto.debra.hytm1'                        : 'abtree-tle'
            # , 'brown_abtree_tm_auto.debra.hytm2'                        : 'abtree-br1'
            , 'brown_ext_abtree_lf.debra'                               : 'abtree-lf'
            , 'brown_sigouin_abtree_kcas.debra'                         : 'abtree-kcas'
            # , 'brown_sigouin_abtree_kcas_htm.debra'                     : 'abtree-kcas+'
            , 'wang_openbwtree'                                         : 'open-bwtree'
            # , 'ellen_ext_bst_lf.debra'                                  : 'ext-bst-lf'
            # , 'guerraoui_ext_bst_ticket.debra'                          : 'ext-bst-locks'
            , 'natarajan_ext_bst_lf.debra'                              : 'ext-bst-lf2'
            # , 'drachsler_pext_bst_lock.debra'                           : 'pext-bst-locks'
            , 'sigouin_int_bst_kcas.debra'                              : 'int-bst-kcas'
            # , 'sigouin_int_bst_kcas_htm.debra'                          : 'int-bst-kcas+'
            # , 'brown_ext_chromatic_lf.debra'                            : 'ext-chromatic-lf'
            , 'bronson_pext_bst_occ.debra'                              : 'pext-avl-occ'
            , 'sigouin_int_avl_kcas.debra'                              : 'int-avl-kcas'
            # , 'sigouin_int_avl_kcas_htm.debra'                          : 'int-avl-kcas+'
            , 'sigouin_int_avl_kcas_validate.debra'                     : 'int-avl-pathcas'
            # , 'sigouin_int_avl_kcas_validate_htm.debra'                 : 'int-avl-pathcas+'
})

host = shell_to_str('hostname').rstrip('\r\n ')
supports_htm = 'rtm' in shell_to_str('lscpu')
supports_papi = (host != 'nasus')

thread_counts = []
if host == 'nasus':
    thread_counts = [64, 256]
elif host == 'jax':
    thread_counts = [48, 190]
else:
    thread_counts = [shell_to_listi('cd ' + get_dir_tools(exp_dict) + ' ; ./get_thread_counts.sh', exit_on_error=True)]

def uses_hardware_tm(long_name):
    return 'hytm' in long_name or 'rhnorec' in long_name or 'hybrid' in long_name or 'htm' in long_name

def is_tm_alg_long(long_name):
    return 'hytm' in long_name or 'norec' in long_name or 'tl2' in long_name

def is_tm_alg_short(short_name):
    return '-rh' in short_name or '-tle' in short_name or '-tl2' in short_name or '-br1' in short_name or 'norec' in short_name

def is_our_alg_short(short_name):
    return '-kcas' in short_name or '-pathcas' in short_name

filtered_algs = dict()
for k,v in unfiltered_algs.items():
    if k and (supports_htm or not uses_hardware_tm(k)):
        filtered_algs[k] = v

def extract_filtered_algs(exp_dict, file_name, field_name):
    result = grep_line(exp_dict, file_name, 'algorithm')
    return filtered_algs[result]

def extract_elapsed_millis(exp_dict, file_name, field_name):
    return get_best_typecast(shell_to_str('cat {} | grep "elapsed milliseconds" | cut -d":" -f2 | tr -d " "'.format(file_name)))

alg_groups = dict()
# alg_groups['abtree_tm']     = list(filter(lambda name: ('abtree' in name or 'bwtree' in name) and (is_tm_alg_short(name) or is_our_alg_short(name))     , filtered_algs.values()))
# alg_groups['abtree_hc']     = list(filter(lambda name: ('abtree' in name or 'bwtree' in name) and (not is_tm_alg_short(name) or is_our_alg_short(name)) , filtered_algs.values()))
# alg_groups['bst_tm']        = list(filter(lambda name: ('bst' in name) and (is_tm_alg_short(name) or is_our_alg_short(name))                            , filtered_algs.values()))
alg_groups['bst_hc']        = list(filter(lambda name: ('bst' in name) and (not is_tm_alg_short(name) or is_our_alg_short(name))                        , filtered_algs.values()))
# alg_groups['avl_tm']        = list(filter(lambda name: ('avl' in name or 'chromatic' in name) and (is_tm_alg_short(name) or is_our_alg_short(name))     , filtered_algs.values()))
alg_groups['avl_hc']        = list(filter(lambda name: ('avl' in name or 'chromatic' in name) and (not is_tm_alg_short(name) or is_our_alg_short(name)) , filtered_algs.values()))

def define_experiment(exp_dict, args):
    set_dir_compile  (exp_dict, os.getcwd() + '/../')
    set_dir_tools    (exp_dict, os.getcwd() + '/../setbench/tools')
    set_dir_run      (exp_dict, os.getcwd() + '/bin')
    set_dir_data     (exp_dict, os.getcwd() + '/data')
    cmd_compile = 'make bin_dir={__dir_run} -j'
    if not supports_papi: cmd_compile += ' has_libpapi=0'

    add_run_param    (exp_dict, 'INS_DEL_FRAC'      , ['0.0 0.0', '50.0 50.0'])
    add_run_param    (exp_dict, 'MAXKEY'            , [20000])
    add_run_param    (exp_dict, 'algorithm'         , list(filtered_algs.keys()))
    add_run_param    (exp_dict, 'thread_pinning'    , ['-pin ' + shell_to_str('cd ' + get_dir_tools(exp_dict) + ' ; ./get_pinning_cluster.sh', exit_on_error=True)])
    add_run_param    (exp_dict, 'millis'            , [3000])
    add_run_param    (exp_dict, 'TOTAL_THREADS'     , [190])
    add_run_param    (exp_dict, '__trials'          , [1, 2])

    cmd_run = 'LD_PRELOAD=/usr/local/lib/libjemalloc.so timeout 360 numactl -i all {__time_cmd} ./{algorithm} -nwork {TOTAL_THREADS} -nprefill {TOTAL_THREADS} -insdel {INS_DEL_FRAC} -k {MAXKEY} -t {millis} {thread_pinning}'
    if args.testing:
        add_run_param(exp_dict, '__trials'          , [1])
        add_run_param(exp_dict, 'TOTAL_THREADS'     , shell_to_listi('cd ' + get_dir_tools(exp_dict) + ' ; ./get_thread_counts_max.sh', exit_on_error=True))
        add_run_param(exp_dict, 'millis'            , [100])
        cmd_compile += ' use_asserts=1'
        cmd_run = cmd_run.replace('-nprefill {TOTAL_THREADS}', '-nprefill 0')

    set_cmd_compile  (exp_dict, cmd_compile)
    set_cmd_run      (exp_dict, cmd_run) ## also adds data_fields to capture the outputs of the `time` command

    add_data_field   (exp_dict, 'alg'               , coltype='TEXT', extractor=extract_filtered_algs)
    add_data_field   (exp_dict, 'validate_result'   , coltype='TEXT', validator=is_equal('success'))
    add_data_field   (exp_dict, 'total_throughput'  , coltype='INTEGER', validator=is_positive)
    add_data_field   (exp_dict, 'PAPI_L3_TCM'       , coltype='REAL')
    add_data_field   (exp_dict, 'PAPI_L2_TCM'       , coltype='REAL')
    add_data_field   (exp_dict, 'PAPI_TOT_CYC'      , coltype='REAL')
    add_data_field   (exp_dict, 'PAPI_TOT_INS'      , coltype='REAL')
    add_data_field   (exp_dict, 'elapsed_millis'    , extractor=extract_elapsed_millis, validator=is_positive)
    add_data_field   (exp_dict, 'MILLIS_TO_RUN'     , coltype='TEXT', validator=is_positive)
    add_data_field   (exp_dict, 'RECLAIM'           , coltype='TEXT')

    ## create pages of plots (one page per combination of algorithm-group and value-field)
    for group_name in alg_groups.keys():
        group = alg_groups[group_name]
        filter_sql = 'alg in ({})'.format(','.join(["'"+x+"'" for x in group]))

        ## create legend to put below each table of plots
        #add_plot_set(exp_dict, name=group_name+'-legend.png', series='alg', x_axis='TOTAL_THREADS', y_axis='total_throughput', filter=filter_sql, plot_line_regions_legend)
        add_plot_set(exp_dict, name=group_name+'-legend.png', series='alg', x_axis='TOTAL_THREADS', y_axis='total_throughput', filter=filter_sql, plot_type='bars', plot_cmd_args='--legend-only --legend-col 3')

        value_fields = ['total_throughput']

        ## add papi outputs where they are not supported
        # if supports_papi: value_fields += ['PAPI_L2_TCM', 'PAPI_L3_TCM', 'PAPI_TOT_CYC', 'PAPI_TOT_INS']

        ## add pages for fields extracted from the time command
        # value_fields += get_time_fields(exp_dict)

        for field in value_fields:
            add_plot_set(exp_dict
                , name=group_name+'_'+field+'-u{INS_DEL_FRAC}-k{MAXKEY}.png'
                , title=field
                , varying_cols_list=['INS_DEL_FRAC', 'MAXKEY']
                , filter=filter_sql
                , series='alg'
                , x_axis='TOTAL_THREADS'
                , y_axis=field
                , plot_type='bars' #plot_line_regions
            )
            add_page_set(exp_dict
                , image_files=group_name+'_'+field+'-u{INS_DEL_FRAC}-k{MAXKEY}.png'
                , legend_file=group_name+'-legend.png'
            )



# ######################################################################
# ## custom plot styling code (unused when 'plotbars' is used above)
# ######################################################################
# import pandas
# import matplotlib as mpl
# mpl.use('Agg')
# import matplotlib.pyplot as plt
# import seaborn as sns
# from run_experiment import get_seaborn_series_styles
# def plot_line_regions(filename, column_filters, data, series_name, x_name, y_name, exp_dict, do_save=True, legend=False, do_close=True):
#     plt.ioff() # stop plots from being shown in jupyter
#     # print('series_name={}'.format(series_name))

#     plot_kwargs = get_seaborn_series_styles(series_name, plot_func=sns.lineplot, exp_dict=exp_dict)
#     plot_kwargs['style'] = series_name
#     plot_kwargs['markersize'] = 12
#     plt.style.use('dark_background')
#     if not legend: plot_kwargs['legend'] = False

#     fig, ax = plt.subplots()
#     sns.lineplot(ax=ax, data=data, x=x_name, y=y_name, hue=series_name, ci=100, **plot_kwargs)
#     plt.tight_layout()

#     if do_save: mpl.pyplot.savefig(filename)
#     # print(data) ; print('## SAVED FIGURE {}'.format(filename))
#     if do_close: plt.close()

# def plot_line_regions_legend(filename, column_filters, data, series_name, x_name, y_name, exp_dict):
#     plot_line_regions(filename, column_filters, data, series_name, x_name, y_name, do_save=False, legend=True, exp_dict=exp_dict, do_close=False)
#     axi = plt.gca()
#     handles, labels = axi.get_legend_handles_labels()

#     fig_legend, axi = plt.subplots()
#     fig_legend.legend(handles[1:], labels[1:], loc='center', frameon=False, ncol=4)

#     axi.xaxis.set_visible(False)
#     axi.yaxis.set_visible(False)
#     axi.axes.set_visible(False)

#     plt.tight_layout()
#     fig_legend.savefig(filename, bbox_inches='tight')
#     plt.close()
