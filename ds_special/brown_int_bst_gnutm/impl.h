#ifndef BROWN_INT_BST_TM_H
#define	BROWN_INT_BST_TM_H

#include <cstdio>
#include "record_manager.h"

#ifndef TRACE
    #define TRACE if(0)
#endif

#define nodeptr Node<K,V> * volatile
template <typename K, typename V>
class Node {
public:
    V value;
    K key;
    nodeptr left;
    nodeptr right;
};

template <typename K, typename V, class Compare, class RecManager>
class brown_int_bst_seq {
private:
PAD;
    RecManager * const recmgr;
PAD;
    nodeptr root;        // actually const
    Compare cmp;
PAD;
    inline nodeptr createNode(const int tid, const K& key, const V& value, nodeptr const left, nodeptr const right);
    int init[MAX_THREADS_POW2] = {0,};
PAD;

    inline V doInsert(const int tid, const K& key, const V& val, bool onlyIfAbsent);
    inline std::pair<nodeptr, nodeptr> doSearch(const int tid, const K& key);
    inline std::pair<nodeptr, nodeptr> doSearchSuccessor(const int tid, nodeptr node);

public:
    const K NO_KEY;
    const V NO_VALUE;
PAD;

    /**
     * This function must be called once by each thread that will
     * invoke any functions on this class.
     *
     * It must be okay that we do this with the main thread and later with another thread!!!
     */
    void initThread(const int tid) {
        if (init[tid]) return; else init[tid] = !init[tid];
        recmgr->initThread(tid);
    }
    void deinitThread(const int tid) {
        if (!init[tid]) return; else init[tid] = !init[tid];
        recmgr->deinitThread(tid);
    }

    void dfsDeallocateBottomUp(nodeptr const u) {
        if (u == NULL) return;
        dfsDeallocateBottomUp(u->left);
        dfsDeallocateBottomUp(u->right);
        const int dummy_tid = 0;
        recmgr->deallocate(dummy_tid, u);
    }

    ~brown_int_bst_seq();
    brown_int_bst_seq(const K _NO_KEY, const V _NO_VALUE, const int numProcesses);

    V insert(const int tid, const K& key, const V& val) { return doInsert(tid, key, val, false); }
    V insertIfAbsent(const int tid, const K& key, const V& val) { return doInsert(tid, key, val, true); }
    V erase(const int tid, const K& key);
    V find(const int tid, const K& key);
    int rangeQuery(const int tid, const K& lo, const K& hi, K * const resultKeys, V * const resultValues) { return 0; }
    bool contains(const int tid, const K& key) { return find(tid, key).second; }

    RecManager * debugGetRecMgr() { return recmgr; }
    nodeptr debug_getEntryPoint() { return root; }
};

template<class K, class V, class Compare, class RecManager>
brown_int_bst_seq<K,V,Compare,RecManager>::brown_int_bst_seq(const K _NO_KEY, const V _NO_VALUE, const int numProcesses)
        : recmgr(new RecManager(numProcesses))
        , NO_KEY(_NO_KEY)
        , NO_VALUE(_NO_VALUE)
{
    VERBOSE DEBUG COUTATOMIC("constructor brown_int_bst_seq"<<std::endl);
    cmp = Compare();
    const int tid = 0;
    initThread(tid);
    root = createNode(tid, NO_KEY, NO_VALUE, NULL, NULL);
}

template<class K, class V, class Compare, class RecManager>
brown_int_bst_seq<K,V,Compare,RecManager>::~brown_int_bst_seq() {
    VERBOSE DEBUG COUTATOMIC("destructor brown_int_bst_seq");
    dfsDeallocateBottomUp(root);
    recmgr->printStatus();
    delete recmgr;
}

template<class K, class V, class Compare, class RecManager>
nodeptr brown_int_bst_seq<K,V,Compare,RecManager>::createNode(const int tid, const K& key, const V& value, nodeptr const left, nodeptr const right) {
    // nodeptr newnode = new Node<K,V>();
    nodeptr newnode = recmgr->template allocate<Node<K,V>>(tid);
    if (newnode == NULL) {
        COUTATOMICTID("ERROR: could not allocate node"<<std::endl);
        exit(-1);
    }
    newnode->key = key;
    newnode->value = value;
    newnode->left = left;
    newnode->right = right;
    return newnode;
}

template<class K, class V, class Compare, class RecManager>
std::pair<nodeptr, nodeptr> brown_int_bst_seq<K,V,Compare,RecManager>::doSearch(const int tid, const K& key) {
    nodeptr p = NULL;
    nodeptr l = root;
    while (true) {
        int currKey = l->key;
        if (currKey != NO_KEY && currKey == key) {
            return std::pair<nodeptr, nodeptr>(l, p);
        }
        nodeptr next = (currKey == NO_KEY || cmp(key, currKey))
                ? l->left : l->right;
        if (!next) break;
        p = l;
        l = next;
    }
    return std::pair<nodeptr, nodeptr>(l, p);
}

template<class K, class V, class Compare, class RecManager>
std::pair<nodeptr, nodeptr> brown_int_bst_seq<K,V,Compare,RecManager>::doSearchSuccessor(const int tid, nodeptr node) {
    assert(node->right);
    nodeptr p = node;
    nodeptr l = node->right;
    while (l->left) {
        p = l;
        l = l->left;
    }
    TRACE printf("    doSearchSuccessor(key %lld) returned nodes l:key%lld and p:key%lld\n", node->key, l->key, p->key);
    return std::pair<nodeptr, nodeptr>(l, p);
}

template<class K, class V, class Compare, class RecManager>
V brown_int_bst_seq<K,V,Compare,RecManager>::find(const int tid, const K& key) {
    auto guard = recmgr->getGuard(tid, true);
    V retval = NO_VALUE;
    synchronized {
        auto ret = doSearch(tid, key);
        auto l = ret.first;
        if (key == l->key) retval = l->value;
    }
    return retval;
}

template<class K, class V, class Compare, class RecManager>
V brown_int_bst_seq<K,V,Compare,RecManager>::doInsert(const int tid, const K& key, const V& val, bool onlyIfAbsent) {
    auto guard = recmgr->getGuard(tid);
    V retval = NO_VALUE;
    synchronized {
        auto ret = doSearch(tid, key);
        auto l = ret.first;
        // if we find the key in the tree already
        if (key == l->key) {
            retval = l->value;
            assert(l->value != NO_VALUE);
            if (!onlyIfAbsent) {
                l->value = val;
            }
            // TRACE printf("doInsert found node %lld and replaced val\n", key, l->key);
        } else {
            auto newLeaf = createNode(tid, key, val, NULL, NULL);
            if (l->key == NO_KEY || cmp(key, l->key)) {
                assert(l->left == NULL);
                TRACE printf("doInsert inserted %lld left of %lld\n", newLeaf->key, l->key);
                TRACE printf("    left=%lld right=%lld\n", (l->left ? l->left->key : -1), (l->right ? l->right->key : -1));
                l->left = newLeaf;
            } else {
                assert(l->right == NULL);
                TRACE printf("doInsert inserted %lld right of %lld\n", newLeaf->key, l->key);
                TRACE printf("    left=%lld right=%lld\n", (l->left ? l->left->key : -1), (l->right ? l->right->key : -1));
                l->right = newLeaf;
            }
        }
    }
    return retval;
}

template<class K, class V, class Compare, class RecManager>
V brown_int_bst_seq<K,V,Compare,RecManager>::erase(const int tid, const K& key) {
    auto guard = recmgr->getGuard(tid);
    nodeptr to_retire = NULL;
    V retval = NO_VALUE;
    synchronized {
        auto ret = doSearch(tid, key);
        auto l = ret.first;
        auto p = ret.second;
        // if we fail to find the key in the tree
        if (key != l->key) {
            TRACE printf("erase finds %lld instead of %lld\n", l->key, key);
            TRACE printf("    left=%lld right=%lld\n", (l->left ? l->left->key : -1), (l->right ? l->right->key : -1));
        } else {
            // figure out which pointer of p points to l
            nodeptr * p_ptr = (l == p->left) ? &p->left : &p->right;
            retval = l->value;

            // leaf delete
            if (l->left == NULL && l->right == NULL) {
                assert(key == l->key);
                TRACE printf("erase LEAF erase %lld\n", l->key);
                TRACE printf("    left=%lld right=%lld\n", (l->left ? l->left->key : -1), (l->right ? l->right->key : -1));
                *p_ptr = NULL;
                to_retire = l;

            // one child delete
            } else if (l->left == NULL || l->right == NULL) {
                assert(key == l->key);
                TRACE printf("erase ONE CHILD erase %lld\n", l->key);
                TRACE printf("    left=%lld right=%lld\n", (l->left ? l->left->key : -1), (l->right ? l->right->key : -1));
                *p_ptr = (l->left) ? l->left : l->right;
                to_retire = l;

            // two child delete
            } else {
                assert(key == l->key);
                TRACE printf("erase TWO CHILD erase %lld\n", l->key);
                TRACE printf("    left=%lld right=%lld\n", (l->left ? l->left->key : -1), (l->right ? l->right->key : -1));
                auto succPair = doSearchSuccessor(tid, l);
                auto succ = succPair.first;
                auto succParent = succPair.second;

                l->value = succ->value;
                l->key = succ->key;

                nodeptr * succParent_ptr = (succ == succParent->left) ? &succParent->left : &succParent->right;
                *succParent_ptr = (succ->left) ? succ->left : succ->right;
                to_retire = succ;
            }
        }
    }
    if (to_retire) recmgr->retire(tid, to_retire);
    return retval;
}

#endif
