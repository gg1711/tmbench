#pragma once

#include <cassert>
#include <ctime>
#include <fstream>
#include <immintrin.h>
#include <iomanip>
#include <iostream>
#include <unordered_set>
using namespace std;

#define MAX_RETRIES 20
#include "tle.h"

#define MAX_THREADS 200
#define MAX_PATH_SIZE 64
#define PADDING_BYTES 128

template <typename K, typename V>
struct Node {
    K key;
    Node<K, V> *left;
    Node<K, V> *right;
    Node<K, V> *parent;
    V value;
};

enum RetCode : int {
    RETRY = 0,
    FAILURE = -1,
    SUCCESS = 1,
};

template <class RecordManager, typename K, typename V>
class InternalHTM {
  private:
    volatile char padding0[PADDING_BYTES];
    const int numThreads;
    const int minKey;
    const long long maxKey;
    volatile char padding4[PADDING_BYTES];
    Node<K, V> *root;
    volatile char padding5[PADDING_BYTES];
    TLEData *tleData = new TLEData();
    volatile char padding6[PADDING_BYTES];

  public:
    InternalHTM(const int _numThreads, const int _minKey, const long long _maxKey);
    ~InternalHTM();
    bool contains(const int tid, const K &key);
    V insertIfAbsent(const int tid, const K &key, const V &value);
    V erase(const int tid, const K &key);
    void printDebuggingDetails();
    Node<K, V> *getRoot();
    void initThread(const int tid);
    void deinitThread(const int tid);
    bool validate();

  private:
    Node<K, V> *createNode(const int tid, Node<K, V> *parent, K key, V value);
    void freeSubtree(const int tid, Node<K, V> *node);
    long validateSubtree(Node<K, V> *node, long smaller, long larger, std::unordered_set<K> &keys, ofstream &graph, ofstream &log, bool &errorFound);
    void internalErase(const int tid, Node<K, V> *node);
    void internalInsert(const int tid, Node<K, V> *parent, const K &key, const V &value);
    int countChildren(const int tid, Node<K, V> *node);
    Node<K, V> *getSuccessor(const int tid, Node<K, V> *node);
    Node<K, V> *search(const int tid, const K &key);
};

template <class RecordManager, typename K, typename V>
Node<K, V> *InternalHTM<RecordManager, K, V>::createNode(const int tid, Node<K, V> *parent, K key, V value) {
    Node<K, V> *node = new Node<K, V>();
    //No node, save for root, should have a NULL parent
    node->key = key;
    node->value = value;
    node->parent = parent;
    node->left = NULL;
    node->right = NULL;
    return node;
}

template <class RecordManager, typename K, typename V>
InternalHTM<RecordManager, K, V>::InternalHTM(const int _numThreads, const int _minKey, const long long _maxKey)
    : numThreads(_numThreads), minKey(_minKey), maxKey(_maxKey) {
    assert(_numThreads < MAX_THREADS);
    root = createNode(0, NULL, (maxKey + 1 & 0x00FFFFFFFFFFFFFF), NULL);
}

template <class RecordManager, typename K, typename V>
InternalHTM<RecordManager, K, V>::~InternalHTM() {
    freeSubtree(0, root);
    delete tleData;
}

template <class RecordManager, typename K, typename V>
inline Node<K, V> *InternalHTM<RecordManager, K, V>::getRoot() {
    return root->left;
}

template <class RecordManager, typename K, typename V>
void InternalHTM<RecordManager, K, V>::initThread(const int tid) {
}

template <class RecordManager, typename K, typename V>
void InternalHTM<RecordManager, K, V>::deinitThread(const int tid) {
}

template <class RecordManager, typename K, typename V>
inline Node<K, V> *InternalHTM<RecordManager, K, V>::getSuccessor(const int tid, Node<K, V> *node) {
    Node<K, V> *succ = node->right;
    while (succ->left != NULL) {
        succ = succ->left;
    }
    return succ;
}

template <class RecordManager, typename K, typename V>
inline bool InternalHTM<RecordManager, K, V>::contains(const int tid, const K &key) {
    bool res;
    TLEGuard tleGuard = TLEGuard(tid, tleData);
    res = search(tid, key)->key == key;
    return res;
}

template <class RecordManager, typename K, typename V>
Node<K, V> *InternalHTM<RecordManager, K, V>::search(const int tid, const K &key) {
    assert(key <= maxKey);
    K currKey;
    Node<K, V> *prev = root;
    Node<K, V> *node = root->left;

    while (true) {
        if (node == NULL) {
            return prev;
        }

        currKey = node->key;
        if (key > currKey) {
            prev = node;
            node = prev->right;
        } else if (key < currKey) {
            prev = node;
            node = prev->left;
        } else {
            return node;
        }
    }
}

template <class RecordManager, typename K, typename V>
inline V InternalHTM<RecordManager, K, V>::insertIfAbsent(const int tid, const K &key, const V &value) {
    V res = 0;
    TLEGuard tleGuard = TLEGuard(tid, tleData);

    auto node = search(tid, key);
    if (node->key == key) {
        res = node->value;
    } else {
        internalInsert(tid, node, key, value);
    }
    return res;
}

template <class RecordManager, typename K, typename V>
void InternalHTM<RecordManager, K, V>::internalInsert(const int tid, Node<K, V> *parent, const K &key, const V &value) {
    Node<K, V> *newNode = createNode(tid, parent, key, value);
    if (key > parent->key) {
        parent->right = newNode;
    } else if (key < parent->key) {
        parent->left = newNode;
    }
}

template <class RecordManager, typename K, typename V>
inline V InternalHTM<RecordManager, K, V>::erase(const int tid, const K &key) {
    V res = 0;
    TLEGuard tleGuard = TLEGuard(tid, tleData);
    auto node = search(tid, key);

    if (node->key == key) {
        res = node->value;
        internalErase(tid, node);
    }
    return res;
}

template <class RecordManager, typename K, typename V>
void InternalHTM<RecordManager, K, V>::internalErase(const int tid, Node<K, V> *node) {
    Node<K, V> *parent = node->parent;

    int numChildren = countChildren(tid, node);
    if (numChildren == 0) {
        if (parent->left == node) {
            parent->left = NULL;
        } else {
            parent->right = NULL;
        }
        delete node;
    } else if (numChildren == 1) {
        Node<K, V> *reroute = node->left != NULL ? node->left : node->right;
        assert(reroute != NULL && reroute->key != 0);

        if (parent->left == node) {
            parent->left = reroute;
        } else {
            parent->right = reroute;
        }
        reroute->parent = parent;
        delete node;
    } else if (numChildren == 2) {
        auto succ = getSuccessor(tid, node);
        assert(succ != NULL && succ->key != 0);

        auto sParent = succ->parent;
        assert(sParent != NULL && sParent->key != 0);

        auto sRight = succ->right;
        assert(sRight == NULL || sRight->key != 0);

        if (sRight != NULL) {
            sRight->parent = sParent;
        }

        if (sParent->left == succ) {
            sParent->left = sRight;
        } else {
            sParent->right = sRight;
        }
        node->value = succ->value;
        node->key = succ->key;
        delete succ;
    }
}

template <class RecordManager, typename K, typename V>
inline int InternalHTM<RecordManager, K, V>::countChildren(const int tid, Node<K, V> *node) {
    return (node->left == NULL ? 0 : 1) + (node->right == NULL ? 0 : 1);
}

template <class RecordManager, typename K, typename V>
bool InternalHTM<RecordManager, K, V>::validate() {
    return true;
}

template <class RecordManager, typename K, typename V>
void InternalHTM<RecordManager, K, V>::printDebuggingDetails() {
    tleData->print();
}

template <class RecordManager, typename K, typename V>
void InternalHTM<RecordManager, K, V>::freeSubtree(const int tid, Node<K, V> *node) {
    if (node == NULL) return;
    freeSubtree(tid, node->left);
    freeSubtree(tid, node->right);
    delete node;
}