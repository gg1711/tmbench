FLAGS =
LDFLAGS =
GPP = g++-10

### if libpapi is installed but you do not want to use it, invoke make with extra argument "has_libpapi=0"
has_libpapi=$(shell setbench/microbench/_check_lib.sh papi)
ifneq ($(has_libpapi), 0)
	FLAGS += -DUSE_PAPI
	LDFLAGS += -lpapi
endif

### if libnuma is installed but you do not want to use it, invoke make with extra argument "has_libnuma=0"
has_libnuma=$(shell setbench/microbench/_check_lib.sh numa)
ifneq ($(has_libnuma), 0)
	FLAGS += -DUSE_LIBNUMA
	LDFLAGS += -lnuma
endif

use_asan=0
ifneq ($(use_asan), 0)
	LDFLAGS += -fsanitize=address -static-libasan
endif

use_asserts=0
ifeq ($(use_asserts), 0)
	FLAGS += -DNDEBUG
endif

use_fopenmp=1
ifeq ($(use_fopenmp), 1)
	FLAGS += -fopenmp
endif

use_timelines=0
ifeq ($(use_timelines), 1)
	FLAGS += -DMEASURE_TIMELINE_STATS
endif

no_optimize=0
ifeq ($(no_optimize), 1)
	#FLAGS += -O0
	FLAGS += -Og -fno-inline-functions -fno-inline-functions-called-once -fno-optimize-sibling-calls
	FLAGS += -fno-default-inline -fno-inline
	FLAGS += -fno-omit-frame-pointer
else
	FLAGS += -O3
endif

FLAGS += -DMAX_THREADS_POW2=512
FLAGS += -DCPU_FREQ_GHZ=2.1 #$(shell ./experiments/get_cpu_ghz.sh)
FLAGS += -DMEMORY_STATS=if\(0\) -DMEMORY_STATS2=if\(0\)
FLAGS += -std=c++17 -g
FLAGS += -DNO_CLEANUP_AFTER_WORKLOAD ### avoid executing data structure destructors, to save teardown time at the end of each trial (useful with massive trees)
# FLAGS += -DRAPID_RECLAMATION
FLAGS += -DRECORD_ABORTS
FLAGS += -DGSTATS_MAX_THREAD_BUF_SIZE=524288
FLAGS += -DDEBRA_ORIGINAL_FREE
FLAGS += -DUSE_TREE_STATS
FLAGS += $(xargs)

LDFLAGS += -Lsetbench/lib
LDFLAGS += -I. -Isetbench/microbench `find setbench/common -type d | sed s/^/-I/`
LDFLAGS += -lpthread
LDFLAGS += -ldl
LDFLAGS += -mrtm

.PHONY: all
all:

bin_dir=bin
dir_guard:
	@mkdir -p $(bin_dir)

clean:
	rm $(bin_dir)/*.out

# FLAGS += $(bin_dir)/death_handler.o -Itmlib/common
# death_handler: dir_guard
# 	$(GPP) tmlib/common/death_handler.cc -c -o $(bin_dir)/death_handler.o -O3 -g -ldl -Wno-unused-result


################################################################################
#### DECIDING WHICH COMBINATIONS OF: {DATA STRUCTURE, RECLAIMER, TM} TO USE
################################################################################

RECLAIMERS=debra

SETBENCH_DATA_STRUCTURES=$(patsubst setbench/ds/%/adapter.h,%,$(wildcard setbench/ds/*/adapter.h))
define create-target-ds-reclaim =
$(1).$(2): dir_guard
	$(GPP) setbench/microbench/main.cpp -o $(bin_dir)/$(1).$(2) -Isetbench/ds/$(1) -DDS_TYPENAME=$(1) -DRECLAIM_TYPE=$(2) $(FLAGS) $(LDFLAGS)
ds-reclaim: $(1).$(2)
endef
$(foreach ds,$(SETBENCH_DATA_STRUCTURES), \
	$(foreach reclaim,$(RECLAIMERS), \
		$(eval $(call create-target-ds-reclaim,$(ds),$(reclaim))) \
	) \
)
# all: ds-reclaim
all: brown_ext_abtree_lf.debra
all: brown_ext_chromatic_lf.debra
all: brown_ext_ist_lf.debra
all: drachsler_pext_bst_lock.debra
all: ellen_ext_bst_lf.debra
all: guerraoui_ext_bst_ticket.debra
all: natarajan_ext_bst_lf.debra
all: bronson_pext_bst_occ.debra
all: howley_int_bst_lf.debra
all: ramachandran_int_bst_lf.debra

# all: brown_ext_abtree_lf.none
# all: brown_ext_chromatic_lf.none
# all: brown_ext_ist_lf.none
# all: drachsler_pext_bst_lock.none
# all: ellen_ext_bst_lf.none
# all: guerraoui_ext_bst_ticket.none
# all: natarajan_ext_bst_lf.none
# all: bronson_pext_bst_occ.none

TM_ALGS=$(patsubst tmlib/%/stm.h,%,$(wildcard tmlib/*/stm.h))
NEW_TM_DATA_STRUCTURES=$(patsubst ds_tm/%/adapter.h,%,$(wildcard ds_tm/*/adapter.h))
define create-target-new-ds-reclaim-tm =
_$(1).$(2).$(3): dir_guard
	$(GPP) setbench/microbench/main.cpp -o $(bin_dir)/$(1).$(2).$(3) -DUSE_GSTATS_USER_HANDLER_H_FILE -Itmlib -Ids_tm/$(1) -DDS_TYPENAME=$(1) -DRECLAIM_TYPE=$(2) -D$(3) $(FLAGS) $(LDFLAGS)
new-ds-reclaim-tm: _$(1).$(2).$(3)
endef
$(foreach ds,$(NEW_TM_DATA_STRUCTURES), \
	$(foreach reclaim,$(RECLAIMERS), \
		$(foreach tm,$(TM_ALGS), \
			$(eval $(call create-target-new-ds-reclaim-tm,$(ds),$(reclaim),$(tm))) \
		) \
	) \
)
all: new-ds-reclaim-tm

NEW_DATA_STRUCTURES=$(patsubst ds/%/adapter.h,%,$(wildcard ds/*/adapter.h))
define create-target-new-ds-reclaim =
_$(1).$(2): dir_guard
	$(GPP) setbench/microbench/main.cpp -o $(bin_dir)/$(1).$(2) -Ids/$(1) -DDS_TYPENAME=$(1) -DRECLAIM_TYPE=$(2) -DUSE_GSTATS_USER_HANDLER_H_FILE $(FLAGS) $(LDFLAGS)
new-ds-reclaim: _$(1).$(2)
endef
$(foreach ds,$(NEW_DATA_STRUCTURES), \
	$(foreach reclaim,$(RECLAIMERS), \
		$(eval $(call create-target-new-ds-reclaim,$(ds),$(reclaim))) \
	) \
)
all: new-ds-reclaim

# ifeq ($(use_asan), 0)
# _brown_int_bst_gnutm.debra: dir_guard
# 	$(GPP) setbench/microbench/main.cpp -o $(bin_dir)/brown_int_bst_gnutm.debra -DUSE_GSTATS_USER_HANDLER_H_FILE -Ids_special/brown_int_bst_gnutm -fgnu-tm -std=c++1z -DDS_TYPENAME=brown_int_bst_gnutm -DRECLAIM_TYPE=debra $(FLAGS) $(LDFLAGS)
# all: _brown_int_bst_gnutm.debra
# endif

_wang_openbwtree: dir_guard
	$(GPP) setbench/microbench/main.cpp ds_special/wang_openbwtree/bwtree.cpp -o $(bin_dir)/wang_openbwtree -Ids_special/wang_openbwtree -DDS_TYPENAME=wang_openbwtree $(FLAGS) -Wno-invalid-offsetof $(LDFLAGS) -latomic
all: _wang_openbwtree
